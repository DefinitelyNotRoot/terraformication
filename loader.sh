#!/usr/bin/env bash
IFS=$'\n' read -d '' -r -a ips < ./ips

for ip in ${ips[@]}; do
    echo $ip
    nohup ssh -i ./key.pem -o StrictHostKeyChecking=no ubuntu@$ip "stress --vm 2 --vm-bytes 100M --timeout 300" > $ip.out &
done
