#!/usr/bin/env bash
> ips
instances=""
ASG="webfarm_asg"
while [ "$instances" = "" ]; do
  instances=$(aws autoscaling describe-auto-scaling-groups --auto-scaling-group-names $ASG  --query AutoScalingGroups[].Instances[].InstanceId --output text)
  sleep 1
done
for ID in $instances;
do
    IP=$(aws ec2 describe-instances --instance-ids $ID  --query Reservations[].Instances[].PrivateIpAddress --output text)
    echo $IP >>ips
done
cat ips

