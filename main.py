import requests
import threading
from concurrent.futures import ProcessPoolExecutor as PoolExecutor



thread_local = threading.local()

def get_session():
    if not hasattr(thread_local, "session"):
        thread_local.session = requests.Session()
    return thread_local.session

def checkPage(url):
    session = get_session()
    with session.get(url, timeout=5) as response:
        print(response.status_code, url, len(response.content))

    
def createMultiThreadingPool(urls):
    with PoolExecutor(max_workers=200) as executor:
            executor.map(checkPage,urls)
    

if __name__ == "__main__":

    with open("./ips") as file:
        ips = file.read().splitlines()
        prefix="http://"
        urls=[prefix+ip for ip in ips] * 2000
    while True:    
        createMultiThreadingPool(urls)
    