terraform {
  required_version = ">= 0.13.5"

  required_providers {
    aws        = ">= 3.36.0"
  }
}
