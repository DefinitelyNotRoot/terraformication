#!/usr/bin/env bash
# set -x
# set -e
IFS=$'\n' read -d '' -r -a ips < ./ips

while true; do
        for ip in ${ips[@]}; do
        url="http://"$ip"/status"
        echo $url 
        curl $url --max-time 20
        done
        aws cloudwatch get-metric-statistics --namespace "AWS/AutoScaling" --start-time $(date -d '1 hour ago' +"%Y-%m-%dT%H:%M:%S%z")  --end-time $(date +"%Y-%m-%dT%H:%M:%S%z") --period 3600 --metric-name GroupInServiceCapacity --statistics Minimum Maximum  --dimensions Name=AutoScalingGroupName,Value=webfarm_asg
        sleep 10
done
