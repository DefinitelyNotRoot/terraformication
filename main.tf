
data "aws_availability_zones" "all" {
    state = "available"
}


resource "aws_vpc" "main" {
  cidr_block       = var.cidr_block
  instance_tenancy = "default"

  tags = {
    Name = "VPC-DEV"
  }
}

resource "aws_security_group" "testSG" {
  name        = "TestSG"
  description = "Allow inbound traffic"
  vpc_id      = aws_vpc.main.id


  ingress {
    description = "HTTP"
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = 80
    to_port     = 80
    protocol    = "TCP"
  }

  ingress {
    description = "HTTP"
    cidr_blocks = ["10.0.0.0/8"]
    from_port   = 22
    to_port     = 22
    protocol    = "TCP"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "TestSG"
  }
}

resource "aws_launch_configuration" "webfarm_config" {
  image_id               = var.image_id
  instance_type          = var.instance_type
  iam_instance_profile   = aws_iam_instance_profile.aws_ec2_s3_cloudwatch_role.name
  security_groups        = [aws_security_group.testSG.id]
  key_name               = var.key_name


  user_data = <<-EOF
              #!/bin/bash
              apt-get  update
              wget 	https://s3.amazonaws.com/amazoncloudwatch-agent/ubuntu/amd64/latest/amazon-cloudwatch-agent.deb
              dpkg -i -E ./amazon-cloudwatch-agent.deb
              wget https://tf-config-settings.s3.eu-central-1.amazonaws.com/amazon-cloudwatch-agent.json -O /opt/aws/amazon-cloudwatch-agent/etc/amazon-cloudwatch-agent.json
              /opt/aws/amazon-cloudwatch-agent/bin/amazon-cloudwatch-agent-ctl -a fetch-config -m ec2 -s -c file:/opt/aws/amazon-cloudwatch-agent/etc/amazon-cloudwatch-agent.json
              apt-get install nginx stress unzip -y
              mkdir -p /var/www/html
              echo "Hello, world!" > /var/www/html/index.html
              rm -f index.nginx-debian.html
              rm -f /etc/nginx/sites-enabled/default
              # echo "Hello, world!" > /var/www/html/index.html
              cat <<"__EOF__" > /etc/nginx/sites-enabled/status.conf 
              server {
                      listen [::]:80 default_server;
                      listen 80 default_server;
                      server_name default_server;
                      keepalive_timeout 0;
                      root /var/www/html;
                      access_log off;

                      location /status {
                          stub_status on;
                      }
                  }
              __EOF__
              systemctl reload nginx 
              EOF
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "webfarm_asg" {
  name = var.asg_name
  launch_configuration = aws_launch_configuration.webfarm_config.name
  vpc_zone_identifier = var.vpc_subnets
  min_size = var.min_server
  max_size = var.max_server
  desired_capacity = var.pref_server
  health_check_type = "EC2"
  enabled_metrics = [ "GroupInServiceCapacity" ]

  tag {
    key = "Name"
    value = "webform_asg"
    propagate_at_launch = true
  }
   lifecycle {
      create_before_destroy = true
    }


  instance_refresh {
    strategy = "Rolling"
    preferences {
      min_healthy_percentage = 50
    }
    triggers = ["tag"]
  }
}


resource "aws_autoscaling_policy" "scale_up_mem" {
  name                   = "webfarm_policy_step_up_mem"
  scaling_adjustment     = 2
  adjustment_type        = "ChangeInCapacity"
  cooldown               = 120
  autoscaling_group_name = aws_autoscaling_group.webfarm_asg.name
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_policy" "scale_down_mem" {
  name                   = "webfarm_policy_step_down_mem"
  scaling_adjustment     = -2
  adjustment_type        = "ChangeInCapacity"
  cooldown               = 120
  autoscaling_group_name = aws_autoscaling_group.webfarm_asg.name
  lifecycle {
    create_before_destroy = true
  }
}


resource "aws_autoscaling_policy" "scale_up_cpu" {
  name                   = "webfarm_policy_step_up_cpu"
  scaling_adjustment     = 2
  adjustment_type        = "ChangeInCapacity"
  cooldown               = 120
  autoscaling_group_name = aws_autoscaling_group.webfarm_asg.name
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_policy" "scale_down_cpu" {
  name                   = "webfarm_policy_step_down_cpu"
  scaling_adjustment     = -2
  adjustment_type        = "ChangeInCapacity"
  cooldown               = 120
  autoscaling_group_name = aws_autoscaling_group.webfarm_asg.name
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_cloudwatch_metric_alarm" "mem-high" {
    alarm_name = "autoscaling_${aws_autoscaling_group.webfarm_asg.name}_mem_policy_increase_capacity"
    comparison_operator = "GreaterThanOrEqualToThreshold"
    evaluation_periods = "2"
    metric_name = "mem_used_percent"
    namespace = "MyCustomNamespace"
    period = "60"
    statistic = "Average"
    threshold = "60"
    alarm_description = "This metric monitors ec2 mem utilization - go alarm when memory => 60% for 60 sec"
    alarm_actions = [
        aws_autoscaling_policy.scale_up_mem.arn
    ]


}

resource "aws_cloudwatch_metric_alarm" "mem-low" {
    alarm_name = "autoscaling_${aws_autoscaling_group.webfarm_asg.name}_mem_policy_derease_capacity"
    comparison_operator = "LessThanOrEqualToThreshold"
    evaluation_periods = "2"
    metric_name = "mem_used_percent"
    namespace = "MyCustomNamespace"
    period = "60"
    statistic = "Average"
    threshold = "50"
    alarm_description = "This metric monitors ec2 mem utilization - go alarm when memory <= 40% for 60 sec"
    alarm_actions = [
        aws_autoscaling_policy.scale_down_mem.arn
    ]

}



resource "aws_cloudwatch_metric_alarm" "cpu-high" {
    alarm_name = "autoscaling_${aws_autoscaling_group.webfarm_asg.name}_cpu_policy_increase_capacity"
    comparison_operator = "GreaterThanOrEqualToThreshold"
    evaluation_periods = "2"
    metric_name = "CPUUtilization"
    namespace = "AWS/EC2"
    period = "60"
    statistic = "Average"
    threshold = "80"
    alarm_description = "This metric monitors ec2 CPU utilization - go alarm when memory => 60% for 60 sec"
    alarm_actions = [
        aws_autoscaling_policy.scale_up_cpu.arn
    ]


}

resource "aws_cloudwatch_metric_alarm" "cpu-low" {
    alarm_name = "autoscaling_${aws_autoscaling_group.webfarm_asg.name}_cpu_policy_derease_capacity"
    comparison_operator = "LessThanOrEqualToThreshold"
    evaluation_periods = "2"
    metric_name = "CPUUtilization"
    namespace = "AWS/EC2"
    period = "60"
    statistic = "Average"
    threshold = "40"
    alarm_description = "This metric monitors ec2 CPU utilization - go alarm when memory <= 40% for 60 sec"
    alarm_actions = [
        aws_autoscaling_policy.scale_down_cpu.arn
    ]

}
resource "aws_iam_role" "aws_ec2_s3_cloudwatch_role" {
  name = "aws_ec2_s3_cloudwatch_role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}
# AWS managed policies
resource "aws_iam_role_policy_attachment" "aws_ec2_s3_cloudwatch_role_policy_attachment" {
  role       = aws_iam_role.aws_ec2_s3_cloudwatch_role.name
  count      = length(var.ec2_cloudwatch_policy)
  policy_arn = element(var.ec2_cloudwatch_policy, count.index)
}

resource "aws_iam_instance_profile" "aws_ec2_s3_cloudwatch_role" {
  name  = "aws_ec2_s3_cloudwatch_role_instance_profile"
  role = aws_iam_role.aws_ec2_s3_cloudwatch_role.name
}