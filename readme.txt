1. change file provider.tf to match your config (AWS CLI credentials) and variable.tf to match your config
2. upload amazon-cloudwatch-agent.json (config file for aws cloudwatch agent) to your s3
2. deploy with terraform apply
3. run getips.sh to get server ips
4. run status.sh in one terminal and loader.sh or main.py in another one (edit loader.sh -> put correct key file)
5. check monitoring in status tab